# Docker Maven & NPM & Semantic Release

Este repositório contém o Dockerfile para construir uma imagem Docker contendo as ferramentas utilizadas para Integração Contínua.

## Referências

- [Docker in Docker - Imagem docker oficial](https://hub.docker.com/_/docker/)
- [JDK 8 - Imagem docker oficial](https://hub.docker.com/_/openjdk/)
- [Maven - Imagem docker oficial](https://hub.docker.com/_/maven/)
- [Node - Imagem docker oficial](https://hub.docker.com/_/node/)
- [Semantic Relese - Documentação](https://semantic-release.gitbook.io/semantic-release/)
- [JQ (JSON Command Line Processor) - Pacote de instalação](https://stedolan.github.io/jq/)

