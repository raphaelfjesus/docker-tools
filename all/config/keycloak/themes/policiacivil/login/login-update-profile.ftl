<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        <i class="mdi mdi-account-card-details"></i> ${msg("loginProfileTitle")}
    <#elseif section = "header">
        <i class="mdi mdi-account-card-details"></i> ${msg("loginProfileTitle")}
    <#elseif section = "form">
        <form id="kc-update-profile-form" class="form update-profile ${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <#if user.editUsernameAllowed>
                <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="username" class="${properties.kcLabelClass!}">${msg("username")}</label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="text" id="username" name="username" value="${(user.username!'')}" class="form-control ${properties.kcInputClass!}"/>
                    </div>
                </div>
            </#if>
            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" name="email" value="${(user.email!'')}" class="form-control ${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" name="firstName" value="${(user.firstName!'')}" class="form-control ${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" name="lastName" value="${(user.lastName!'')}" class="form-control ${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('cpf',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="cpf" class="${properties.kcLabelClass!}">${msg("cpf")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="cpf" name="cpf" value="${(user.attributes.cpf!'')}" class="form-control ${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('mobilePhone',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="mobilePhone" class="${properties.kcLabelClass!}">${msg("mobilePhone")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="number" id="mobilePhone" name="mobilePhone" value="${(user.attributes.mobilePhone!'')}" class="form-control ${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="update-profile-field ${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('gender',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="gender" class="${properties.kcLabelClass!}">${msg("gender")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <select id="gender" name="gender" value="${(user.attributes.gender!'')}" class="form-control ${properties.kcInputClass!}">
                        <option value="">Selecione...</option>
                        <option value="m" label="${msg("male")}">${msg("male")}</option>
                        <option value="f" label="${msg("female")}">${msg("female")}</option>
                    </select>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} row update-profile-button-container">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!} col-xs-6 xs-xs-offset-6 col-sm-4 col-sm-offset-8">
                    <input class="btn btn-primary btn-flat btn-block ${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
