<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "title">
        <i class="mdi mdi-emoticon-sad"></i> ${msg("errorTitle")}
    <#elseif section = "header">
        <i class="mdi mdi-emoticon-sad"></i> ${msg("errorTitleHtml")?no_esc}
    <#elseif section = "form">
        <div id="kc-error-message">
            <p class="instruction">${message.summary}</p>
            <#if client?? && client.baseUrl?has_content>
                <p><a id="backToApplication" href="${client.baseUrl}"><i class="mdi mdi-arrow-left-bold-circle-outline"></i>&nbsp;&nbsp;${msg("backToApplication")}</a></p>
            </#if>
        </div>
    </#if>
</@layout.registrationLayout>
