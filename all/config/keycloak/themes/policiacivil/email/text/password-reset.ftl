<#ftl output_format="plainText">
Use este link para redefinir sua senha. O link é válido apenas por ${linkExpiration} minutos.

Polícia Civil do Estado de São Paulo
${realmName}

**************************
Olá <#if user.firstName?has_content>${user.firstName}<#else>${user.username}</#if>,
**************************

Você recentemente solicitou a redefinição de sua senha para sua conta no ${realmName}. Copie e cole a URL abaixo em seu navegador web para redefiní-la. Essa redefinição de senha é válida apenas pelos próximos ${linkExpiration} minutos.

${link}

Por segurança, essa solicitação foi recebida de um dispositivo NÃO IDENTIFICADO usando um navegador NÃO IDENTIFICADO. Se você não solicitou uma redefinição de senha, ignore este e-mail ou entre em contato com o suporte se tiver dúvidas.

Obrigado,
Equipe ${realmName}


© 2018 ${realmName}. Todos os direitos reservados.

