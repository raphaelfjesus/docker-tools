<#ftl output_format="plainText">
Use este link para verificar o seu endereço de e-mail. O link é válido apenas por ${linkExpiration} minutos.

Polícia Civil do Estado de São Paulo
${realmName}

**************************
Olá <#if user.firstName?has_content>${user.firstName}<#else>${user.username}</#if>,
**************************

Alguém criou uma conta no ${realmName} com este endereço de e-mail. Se foi você, copie e cole a URL abaixo em seu navegador web para verificar o seu endereço de e-mail. Essa verificação de e-mail é válida apenas pelos próximos ${linkExpiration} minutos.

${link}

Por segurança, essa solicitação foi recebida de um dispositivo NÃO IDENTIFICADO usando um navegador NÃO IDENTIFICADO. Se não foi você que criou esta conta, basta ignorar esta mensagem ou entre em contato com o suporte se tiver dúvidas.

Obrigado,
Equipe ${realmName}


© 2018 ${realmName}. Todos os direitos reservados.

