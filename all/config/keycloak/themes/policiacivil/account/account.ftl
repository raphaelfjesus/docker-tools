<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("editAccountHtmlTitle")}</h2>
        </div>
        <div class="col-md-2 subtitle">
            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
        </div>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post" id="accountForm">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <#if !realm.registrationEmailAsUsername>
            <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
                <div class="col-sm-2 col-md-2">
                    <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                </div>
            </div>
        </#if>

	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.idDevice" class="control-label">${msg("idDevice")}</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.idDevice" name="user.attributes.idDevice"
               <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.attributes.idDevice!'')}"/>
	   </div>
	</div>


        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-2">
            <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="email" name="email" autofocus value="${(account.email!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('user.attributes.gender','has-error')}">
            <div class="col-sm-2 col-md-2">
              <label for="user.attributes.gender" class="control-label">${msg("gender")}</label>
            </div>
            <div class="col-sm-10 col-md-10">
	        <select class="form-control" id="user.attributes.gender" name="user.attributes.gender" value="${(account.attributes.gender!'')}">
                   <option value="">Selecione...</option>
                   <#if (account.attributes.gender!'')='m'>
                      <option value="m" label="${msg("male")}" selected>${msg("male")}</option>
                   <#else>
                      <option value="m" label="${msg("male")}">${msg("male")}</option>
                   </#if>
                   <#if (account.attributes.gender!'')='f'>			
                      <option value="f" label="${msg("female")}" selected>${msg("female")}</option>
                   <#else>
                      <option value="f" label="${msg("female")}">${msg("female")}</option>
                   </#if>
                </select>
            </div>
	</div>

        <div class="form-group ${messagesPerField.printIfExists('user.attributes.cpf','has-error')}">
           <div class="col-sm-2 col-md-2">
               <label for="user.attributes.cpf" class="control-label">${msg("cpf")}</label>
           </div>

           <div class="col-sm-10 col-md-10">
               <input type="text" class="form-control" id="user.attributes.cpf" name="user.attributes.cpf" value="${(account.attributes.cpf!'')}"/>
           </div>
        </div>


        <div class="form-group ${messagesPerField.printIfExists('user.attributes.cargo','has-error')}">
           <div class="col-sm-2 col-md-2">
               <label for="user.attributes.cargo" class="control-label">${msg("cargo")}</label>
           </div>

           <div class="col-sm-10 col-md-10">
               <input type="text" class="form-control" id="user.attributes.cargo" name="user.attributes.cargo" value="${(account.attributes.cargo!'')}"/>
           </div>
        </div>


        <div class="form-group ${messagesPerField.printIfExists('user.attributes.mobilePhone','has-error')}">
           <div class="col-sm-2 col-md-2">
               <label for="user.attributes.mobilePhone" class="control-label">${msg("mobilePhone")}</label>
           </div>

           <div class="col-sm-10 col-md-10">
               <input type="text" class="form-control" id="user.attributes.mobilePhone" name="user.attributes.mobilePhone" value="${(account.attributes.mobilePhone!'')}"/>
           </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('user.attributes.idDelegacia','has-error')}">
           <div class="col-sm-2 col-md-2">
               <label for="user.attributes.idDelegacia" class="control-label">${msg("idDelegacia")}</label>
           </div>

           <div class="col-sm-10 col-md-10">
               <input type="text" class="form-control" id="user.attributes.idDelegacia" name="user.attributes.idDelegacia" value="${(account.attributes.idDelegacia!'10101')}"/>
           </div>
        </div>

        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${msg("backToApplication")?no_esc}/a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                </div>
            </div>
        </div>

    </form>

</@layout.mainLayout>
