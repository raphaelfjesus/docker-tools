#!/bin/bash

set -e


# first arg is `-f` or `--some-option` or first arg is `something.conf`
if [ "${1#-}" != "$1" ] || [ "${1%.conf}" != "$1" ]; then
	set -- redis-server "$@"
fi

# Drop root privileges if we are running redis
# allow the container to be started with `--user`
if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
	# Change the ownership of user-mutable directories to redis
	chown -R redis:redis /usr/share/redis/data

	set -- su-exec redis "$0" "$@"
fi

exec "$@"
