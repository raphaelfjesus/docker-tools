#!/bin/bash

if [ $KEYCLOAK_USERNAME ] && [ $KEYCLOAK_PASSWORD ]; then
  /opt/jboss/keycloak/bin/add-user-keycloak.sh --user $KEYCLOAK_USERNAME --password $KEYCLOAK_PASSWORD
fi

if [ "$JAVA_OPTS" != "" ]; then
  export JAVA_OPTS="$JAVA_OPTS"
fi

if [ "$KEYCLOAK_HOSTNAME" != "" ]; then
  SYS_PROPS="-Dkeycloak.hostname.provider=fixed -Dkeycloak.hostname.fixed.hostname=$KEYCLOAK_HOSTNAME"

  if [ "$KEYCLOAK_HTTP_PORT" != "" ]; then
    SYS_PROPS+=" -Dkeycloak.hostname.fixed.httpPort=$KEYCLOAK_HTTP_PORT"
  fi

  if [ "$KEYCLOAK_HTTPS_PORT" != "" ]; then
    SYS_PROPS+=" -Dkeycloak.hostname.fixed.httpsPort=$KEYCLOAK_HTTPS_PORT"
  fi
fi

if [ "$KEYCLOAK_IMPORT" == "dipol-dev" ]
then
  SYS_PROPS+=" -Dkeycloak.import=/opt/jboss/realms/realm-dipol-dev.json"
elif [ "$KEYCLOAK_IMPORT" == "dipol-prod" ];
then
  SYS_PROPS+=" -Dkeycloak.import=/opt/jboss/realms/realm-dipol-prod.json"
elif [ "$KEYCLOAK_IMPORT" ]; 
then
  SYS_PROPS+=" -Dkeycloak.import=$KEYCLOAK_IMPORT"
fi

if [ -z "$BIND" ]; then
  BIND=$(hostname -i)
fi
if [ -z "$BIND_OPTS" ]; then
  for BIND_IP in $BIND
  do
    BIND_OPTS+=" -Djboss.bind.address=$BIND_IP -Djboss.bind.address.private=$BIND_IP "
  done
fi
SYS_PROPS+=" $BIND_OPTS"

if [ "$KEYCLOAK_PORT" != "" ]; then
  SYS_PROPS+=" -Djboss.http.port=$KEYCLOAK_PORT"
fi

# If the "-c" parameter is not present, append the HA profile.
if echo "$@" | egrep -v -- "-c "; then
    SYS_PROPS+=" -c standalone-ha.xml"
fi

# Default to H2 if DB type not detected
if [ "$KEYCLOAK_DATABASE_VENDOR" == "postgres" ]; then
  export KEYCLOAK_DATABASE_VENDOR="postgres"
	KEYCLOAK_DATABASE_NAME="PostgreSQL"
elif [ "$KEYCLOAK_DATABASE_VENDOR" == "sqlserver" ]; then
  export KEYCLOAK_DATABASE_VENDOR="sqlserver"
	KEYCLOAK_DATABASE_NAME="SQL Server"
else
  export KEYCLOAK_DATABASE_VENDOR="h2"
	KEYCLOAK_DATABASE_NAME="Embedded H2"
fi

# Append '?' in the beggining of the string if JDBC_PARAMS value isn't empty
export JDBC_PARAMS=$(echo ${JDBC_PARAMS} | sed '/^$/! s/^/?/')

echo "System Properties: $SYS_PROPS"
echo "========================================================================="
echo ""
echo "  Using $KEYCLOAK_DATABASE_NAME database"
echo ""
echo "========================================================================="
echo ""

# Change database
if [ "$KEYCLOAK_DATABASE_VENDOR" != "h2" ]; then
  /opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/databases/$KEYCLOAK_DATABASE_VENDOR/standalone-configuration.cli
  rm -rf /opt/jboss/keycloak/standalone/configuration/standalone_xml_history

  /opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/databases/$KEYCLOAK_DATABASE_VENDOR/standalone-ha-configuration.cli
  rm -rf standalone/configuration/standalone_xml_history/current/*

  #/opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/databases/db2/standalone-configuration.cli
  #rm -rf /opt/jboss/keycloak/standalone/configuration/standalone_xml_history

  #/opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/databases/db2/standalone-ha-configuration.cli
  #rm -rf standalone/configuration/standalone_xml_history/current/*
fi

# System properties
/opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/system-properties/standalone-configuration.cli
/opt/jboss/keycloak/bin/jboss-cli.sh --file=/opt/jboss/tools/cli/system-properties/standalone-ha-configuration.cli

#/opt/jboss/tools/x509.sh
#/opt/jboss/tools/jgroups.sh $JGROUPS_DISCOVERY_PROTOCOL $JGROUPS_DISCOVERY_PROPERTIES

exec /opt/jboss/keycloak/bin/standalone.sh $SYS_PROPS $@
exit $?